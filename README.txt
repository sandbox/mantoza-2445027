CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Installation
 * Known issues
 * Support
 * Sponsorship


INTRODUCTION
------------

Commerce Cardinity is Drupal Commerce module that integrates Cardinity payment
gateway into your Drupal Commerce shop.


FEATURES
--------

1. Direct payment
2. 3-D Secure support.
3. card on file functionality that allows for you securely to charge a client
card without having to deal with the huge hassle of storing credit card numbers.

Note that to enable the card on file functionality you need to install
the 2.x version of the commerce_cardonfile module.


INSTALLATION
------------

  1. Copy the 'commerce_cardinity' folder into the modules directory
    usually: '/sites/all/modules/'.

  2. In your Drupal site, enable the module under Administration -> Modules
    The module will be in the group Commerce - Payment.

  3. Get a Cardinity account

  4. Configure the payment rule at admin/commerce/config/payment-methods
    with your keys.
